### What is this repository for? ###

* This is the app used during the June 7th demo of the RouteMe2 app for a cleaner and more commented version see:
https://bitbucket.org/routeme2/routeme2appupdated

### How do I get set up? ###
To run this, you must have an android device with BLE, GPS, and Location capabilities....
First download all files into android studios.
build the app on your device
If the app crashes, you may have to manually go into system preferences to give the app location privileges 

### Who do I talk to? ###

Anthony Chong
anthonymchong@gmail.com